using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.Rendering;

namespace UnityEngine.XR.ARFoundation.Samples
{
    /// <summary>
    /// Listens for touch events and performs an AR raycast from the screen touch point.
    /// AR raycasts will only hit detected trackables like feature points and planes.
    ///
    /// If a raycast hits a trackable, the <see cref="placedPrefab"/> is instantiated
    /// and moved to the hit position.
    /// </summary>
    [RequireComponent(typeof(ARRaycastManager))]
    public class PlaceOnPlane : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("Instantiates this prefab on a plane at the touch location.")]
        GameObject m_PlacedPrefab;
        [SerializeField]
        [Tooltip("The ARCameraManager which will produce frame events containing light estimation information.")]
        ARCameraManager m_CameraManager;
        [SerializeField] private ARPlaneManager m_PlaneManager;
        [SerializeField] private GameObject arCameraObject;
        [SerializeField] private ARSession m_ArSession;
        [SerializeField] private Button resetButton;
        [SerializeField] private Text resetButtonStatus;
        [SerializeField] private Text touchStatus;
        [SerializeField] private Canvas cameraCanvas;

        /// <summary>
        /// The prefab to instantiate on touch.
        /// </summary>
        public GameObject placedPrefab
        {
            get { return m_PlacedPrefab; }
            set { m_PlacedPrefab = value; }
        }


        public ARCameraManager cameraManager
        {
            get { return m_CameraManager; }
            set
            {
                if (m_CameraManager == value)
                    return;
            }
        }

        /// <summary>
        /// The object instantiated as a result of a successful raycast intersection with a plane.
        /// </summary>
        public GameObject spawnedObject { get; private set; }
        public Camera ARContentCamera;


        // AR Camera Parameter.
        private Color? colorCorrection { get; set; }
        private Matrix4x4 projectionMatrix { get; set; }
        private Matrix4x4 modelMatrix { get; set; }
        private Matrix4x4 viewMatrix { get; set; }
        private Matrix4x4 modelViewMatrix { get; set; }
        private long? timestampNs { get; set; }
        //private IEnumerator StartShowARCameraParameter = null;
        private bool _isStartShowCameraFrame = CameraFrame.isStartShowCameraFrame;


        void Awake()
        {
            m_RaycastManager = GetComponent<ARRaycastManager>();
            m_Light = GetComponent<Light>();
            ARContentCamera.gameObject.SetActive(false);
            resetButton.onClick.AddListener(ResetArSession);
        }


        private void OnEnable()
        {
            if (m_CameraManager != null)
            {
                m_CameraManager.frameReceived += OnFrameReceived;
            }
        }


        private void OnDisable()
        {
            if (m_CameraManager != null)
            {
                m_CameraManager.frameReceived -= OnFrameReceived;
            }
        }


        IEnumerator Start()
        {
            if ((ARSession.state == ARSessionState.None) ||
                (ARSession.state == ARSessionState.CheckingAvailability))
            {
                yield return ARSession.CheckAvailability();
            }

            if (ARSession.state == ARSessionState.Unsupported)
            {
                // Start some fallback experience for unsupported devices
                Debug.LogError("AR foundation is not supported on this device.");
            }
            else
            {
                // Start the AR session
                Debug.Log("AR foundation is supported on this device.");
                m_ArSession.enabled = true;
            }
        }


        private void OnFrameReceived(ARCameraFrameEventArgs eventArgs)
        {
            if (eventArgs.projectionMatrix.HasValue)
            {
                projectionMatrix = eventArgs.projectionMatrix.Value;
            }
            else
            {
                Debug.Log("Can not get projection matrix.");
            }

            // Check color correction has value.
            if (eventArgs.lightEstimation.colorCorrection.HasValue)
            {
                colorCorrection = eventArgs.lightEstimation.colorCorrection.Value;
                m_Light.color = colorCorrection.Value;
            }
            else
            {
                Color _colorCorrection = new Color(1f, 1f, 1f, 1f);
                colorCorrection = _colorCorrection;
            }
        }


        bool TryGetTouchPosition(out Vector2 touchPosition)
        {
            if (Input.touchCount > 0)
            {
                touchPosition = Input.GetTouch(0).position;
                return true;
            }

            touchPosition = default;
            return false;
        }


        void Update()
        {
            // when user touch screen one second,
            // touchCount will be increase about fifty times.
            if (Input.touchCount == 1)
            {
                // Get user first touch position.
                var touch = Input.GetTouch(0);
                if (touch.phase != TouchPhase.Began)
                    return;

                // Get touch position.
                if (!TryGetTouchPosition(out Vector2 touchPosition))
                    return;

                // If user hit a plane, show model and anchor's model matrix.
                if (m_RaycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon))
                {
                    // Raycast hits are sorted by distance, so the first one
                    // will be the closest hit.
                    var hitPose = s_Hits[0].pose;

                    if (spawnedObject == null)
                    {
                        spawnedObject = Instantiate(m_PlacedPrefab, hitPose.position, hitPose.rotation);
                        touchStatus.text = "Touch status : First.";


                        //Debug.Log("Start coroutine to get AR camera parameter.");
                        //StartShowARCameraParameter = GetARCameraParameter();
                        //StartCoroutine(StartShowARCameraParameter);

                        _isStartShowCameraFrame = true;
                        CameraFrame.isStartShowCameraFrame = _isStartShowCameraFrame;

                        ClearARPlane();

                        //m_CameraManager.gameObject.SetActive(false);
                        //spawnedTempleModelObject = Instantiate(templeModel, new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
                        //spawnedTempleModelObject = Instantiate(templeModel, hitPose.position, hitPose.rotation);
                    }
                    else
                    {
                        touchStatus.text = "Touch status : Second.";
                    }
                }
            }
        }


        //IEnumerator GetARCameraParameter()
        //{
        //    while (true)
        //    {
        //        resetButtonStatus.text = $"Reset button status : Deactivate";

        //        // Calculate modelview matrix.
        //        modelMatrix = spawnedObject.transform.localToWorldMatrix;
        //        viewMatrix = Matrix4x4.Inverse(Matrix4x4.TRS(arCameraObject.transform.position, arCameraObject.transform.rotation, new Vector3(1, 1, -1)));
        //        modelViewMatrix = viewMatrix * modelMatrix;

        //        Debug.Log($"==========================================\nModelViewMatrix: {modelViewMatrix}\nProjectionMatrix: {projectionMatrix}\nColorCorrection: {colorCorrection}\n==========================================");

        //        ApplyData();

        //        yield return new WaitForSeconds(0.33f);
        //    }
        //}


        public void GetARCameraParameter()
        {
            resetButtonStatus.text = $"Reset button status : Deactivate";

            // Calculate modelview matrix.
            modelMatrix = spawnedObject.transform.localToWorldMatrix;
            viewMatrix = Matrix4x4.Inverse(Matrix4x4.TRS(arCameraObject.transform.position, arCameraObject.transform.rotation, new Vector3(1, 1, -1)));
            modelViewMatrix = viewMatrix * modelMatrix;

            Debug.Log($"==========================================\nModelViewMatrix: {modelViewMatrix}\nProjectionMatrix: {projectionMatrix}\nColorCorrection: {colorCorrection}\n==========================================");

            ApplyData();
        }


        private void ApplyData()
        {
            ARContentCamera.gameObject.SetActive(true);
            ARContentCamera.projectionMatrix = projectionMatrix;
            ARContentCamera.worldToCameraMatrix = viewMatrix;
        }


        private void ClearARPlane()
        {
            foreach (var trackable in m_PlaneManager.trackables)
            {
                trackable.gameObject.SetActive(false);
            }
            m_PlaneManager.enabled = false;
        }


        public void ResetArSession()
        {
            m_ArSession.Reset();
            Destroy(spawnedObject);
            //Destroy(spawnedTempleModelObject);
            m_CameraManager.enabled = true;
            m_PlaneManager.enabled = true;
            ARContentCamera.gameObject.SetActive(false);
            cameraCanvas.gameObject.SetActive(false);
            CameraFrame.isStartShowCameraFrame = false;
            //StopCoroutine(StartShowARCameraParameter);

            resetButtonStatus.text = $"Reset button status : Activate";
            touchStatus.text = "Touch status : null";
        }


        static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
        Light m_Light;
        ARRaycastManager m_RaycastManager;
    }
}
