using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System;

namespace UnityEngine.XR.ARFoundation.Samples
{
    public class CameraFrame : MonoBehaviour
    {
        [SerializeField] private PlaceOnPlane placeOnPlane;
        [SerializeField] private ARCameraManager m_CameraManager;
        [SerializeField] private ARSession m_ArSession;
        [SerializeField] private RawImage m_RawCameraImage;
        [SerializeField] private Text deviceText;
        [SerializeField] private Text isStartShowCameraFrameStatus;
        [SerializeField] private Text displaySize;
        [SerializeField] private Button closeRawCameraImageButton;
        [SerializeField] private Button openRawCameraImageButton;
        [SerializeField] private Button closeCameraButton;
        [SerializeField] private Button openCameraButton;
        [SerializeField] private Canvas cameraCanvas;

        public Camera CameraFrameCamera;
        private Texture2D copy_CameraTexture = null;
        public static bool isStartShowCameraFrame = false;
        public bool matchFrameRateRequested { get; set; }


        void OnBeforeRender() => UpdateCameraImage();
        

        public ARCameraManager cameraManager
        {
            get { return m_CameraManager; }
            set
            {
                if (m_CameraManager == value)
                    return;
            }
        }

        void Awake()
        {
            CameraFrameCamera.gameObject.SetActive(false);
            m_RawCameraImage.gameObject.SetActive(false);
            //m_RawCameraImage.GetComponent<RectTransform>().sizeDelta = new Vector2(640, 480);
            m_RawCameraImage.GetComponent<RectTransform>().sizeDelta = new Vector2(Display.main.systemWidth, Display.main.systemHeight);
            closeRawCameraImageButton.onClick.AddListener(CloseRawCameraImage);
            openRawCameraImageButton.onClick.AddListener(OpenRawCameraImage);
            openCameraButton.onClick.AddListener(OpenCamera);
            closeCameraButton.onClick.AddListener(CloseCamera);
        }


        private void OnEnable()
        {
#if UNITY_IPHONE
            if (m_CameraManager != null)
            {
                m_CameraManager.frameReceived += OnFrameReceived;
            }
            deviceText.text = "Device : IOS";
            displaySize.text = $"Display Size : {Display.main.systemWidth}x{Display.main.systemHeight}";
#endif

#if UNITY_ANDROID
            //if (m_ArSession.matchFrameRateRequested)
            //{
            //    Application.onBeforeRender += OnBeforeRender;
            //}
            if (m_CameraManager != null)
            {
                m_CameraManager.frameReceived += OnFrameReceived;
            }
            deviceText.text = "Device : Android";
            displaySize.text = $"Display size : {Display.main.systemWidth}x{Display.main.systemHeight}";
#endif
        }

        private void OnDisable()
        {
#if UNITY_IPHONE
            if (m_CameraManager != null)
            {
                m_CameraManager.frameReceived -= OnFrameReceived;
            }
            deviceText.text = "Device : IOS";
#endif

#if UNITY_ANDROID
            //if (m_ArSession.matchFrameRateRequested)
            //{
            //    Application.onBeforeRender -= OnBeforeRender;
            //}
            if (m_CameraManager != null)
            {
                m_CameraManager.frameReceived -= OnFrameReceived;
            }
            deviceText.text = "Device : Android";
#endif
        }

        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {

        }

        unsafe void UpdateCameraImage()
        {
            if (!cameraManager.TryAcquireLatestCpuImage(out XRCpuImage image))
            {
                return;
            }

            var format = TextureFormat.RGBA32;

            if (m_CameraTexture == null || m_CameraTexture.width != image.width || m_CameraTexture.height != image.height)
            {
                m_CameraTexture = new Texture2D(image.width, image.height, format, false);
            }

            if (copy_CameraTexture == null)
            {
                copy_CameraTexture = new Texture2D(image.width, image.height, format, false);
            }

            var conversionParams = new XRCpuImage.ConversionParams(image, format, m_Transformation);

            var rawTextureData = m_CameraTexture.GetRawTextureData<byte>();
            try
            {
                image.Convert(conversionParams, new IntPtr(rawTextureData.GetUnsafePtr()), rawTextureData.Length);

                placeOnPlane.GetARCameraParameter();

                copy_CameraTexture.SetPixels(m_CameraTexture.GetPixels());
                copy_CameraTexture.Apply();

                m_RawCameraImage.texture = copy_CameraTexture;
            }
            finally
            {
                image.Dispose();
            }

            m_CameraTexture.Apply();
        }

        XRCpuImage.Transformation m_Transformation = XRCpuImage.Transformation.MirrorY;

        private void OnFrameReceived(ARCameraFrameEventArgs eventArgs)
        {
            isStartShowCameraFrameStatus.text = $"Show camera frame : {isStartShowCameraFrame}";
            
            if (isStartShowCameraFrame)
            {
                cameraCanvas.gameObject.SetActive(true);
                CameraFrameCamera.gameObject.SetActive(true);
                m_RawCameraImage.gameObject.SetActive(true);
                UpdateCameraImage();
            }
            else
            {
                m_RawCameraImage.gameObject.SetActive(false);
            }
            
        }


        public void CloseRawCameraImage()
        {
            closeRawCameraImageButton.gameObject.SetActive(false);
            openRawCameraImageButton.gameObject.SetActive(true);
            isStartShowCameraFrame = false;
        }

        public void OpenRawCameraImage()
        {
            closeRawCameraImageButton.gameObject.SetActive(true);
            openRawCameraImageButton.gameObject.SetActive(false);
            isStartShowCameraFrame = true;
        }

        public void CloseCamera()
        {
            closeCameraButton.gameObject.SetActive(false);
            openCameraButton.gameObject.SetActive(true);
            CameraFrameCamera.gameObject.SetActive(false);
            cameraCanvas.gameObject.SetActive(false);
        }

        public void OpenCamera()
        {
            closeCameraButton.gameObject.SetActive(true);
            openCameraButton.gameObject.SetActive(false);
            CameraFrameCamera.gameObject.SetActive(true);
            cameraCanvas.gameObject.SetActive(true);
        }


        Texture2D m_CameraTexture;
    }
}
